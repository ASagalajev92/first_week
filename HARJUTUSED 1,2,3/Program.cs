﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HARJUTUSED_1_2_3
{
    class Program
    {


        //public static void Shuffle(int[] arr)
        //{
        //    // создаем экземпляр класса Random для генерирования случайных чисел
        //    Random rand = new Random();

        //    for (int i = arr.Length - 1; i >= 1; i--)
        //    {
        //        int j = rand.Next(i + 1);

        //        int tmp = arr[j];
        //        arr[j] = arr[i];
        //        arr[i] = tmp;
        //    }
        //}
        static void Main(string[] args)
        {


            #region REPLACE PART OR WORD IN STRING

            //string text = "хороший день";

            //text = text.Replace("хороший", "плохой");
            //Console.WriteLine(text); // Заменили хороший на плохой ! 

            //text = text.Replace("о", "");
            //Console.WriteLine(text); // Заменили букву о везде на пустоту ! 

            #endregion

            #region REMOVE FROM STRING

            //string text = "Хороший день";
            //// индекс последнего символа
            //int ind = text.Length - 1;
            //// вырезаем последний символ
            //text = text.Remove(ind);
            //Console.WriteLine(text);

            //// вырезаем первые 2 символа
            //text = text.Remove(0, 2); 

            #endregion

            #region INSER STRING TO STRING

            //string text = "Хороший день";
            //string subString = "замечательный ";

            //text = text.Insert(8, subString); //Vstavljajem tekst ukazannij v peremennoi subStrings pered 8 indeksom na4inaja ot 0 !!!
            //Console.WriteLine(text); 

            #endregion

            #region SUBSTRING 

            //string text = "AB_CDEFSEPST";

            //text = text.Substring(1); // обрезаем начиная с 1 символа
            //Console.WriteLine(text);  

            //text = text.Substring(1, text.Length - 3); // обрезаем с 1 до последних 3 символов
            //Console.WriteLine(text); ??? 

            #endregion

            #region RANDOM STRING ARRAY
            //Random rnd = new Random();
            //string[] malePetNames = { "Rufus", "Bear", "Dakota", "Fido",
            //                    "Vanya", "Samuel", "Koani", "Volodya",
            //                    "Prince", "Yiska" };
            //string[] femalePetNames = { "Maggie", "Penny", "Saya", "Princess",
            //                      "Abby", "Laila", "Sadie", "Olivia",
            //                      "Starlight", "Talla" };
            //// Generate random indexes for pet names.
            //int mIndex = rnd.Next(malePetNames.Length);
            //int fIndex = rnd.Next(femalePetNames.Length);

            //// Display the result.
            //Console.WriteLine("Suggested pet name of the day: ");
            //Console.WriteLine("   For a male:     {0}", malePetNames[mIndex]);
            //Console.WriteLine("   For a female:   {0}", femalePetNames[fIndex]);  
            #endregion




            #region HARJUTUS 0

            // tee funktsioon string ToProper(string) { ... } 
            //   (nдitan, kuidas teha jдrgmine nдdal selgitan)
            //   millele antakse ette string ja mis vastuseks annab stringi
            //   tee nii, et etteantud string(HENN, hENN.henn), tagastatakse
            //   suure algustдhe kujul(Henn)
            //   kontrolli, kas toimib
            //   tee nььd teine, mis oskab ka mitmeosalise nimega sama teha
            //   kui sul aega ja viitsimist on, tee nii, et nii tьhikuga kui
            //   sidekriipsuga toimiks
            //   ДRA VДGA SIIA TAKERDU -esmaspдeval vaatame kindlasti ьle
            //   aga vхibolla juba tдna - kuidas aega jддb

            //    string eesnimi = Console.ReadLine();
            //    string perekonnanimi = Console.ReadLine();

            //    Console.WriteLine(ToProper($"Esialgsed andmed {eesnimi} {perekonnanimi} "));

            //    string a = ToProper($"{eesnimi}");
            //    string b = ToProper($"{perekonnanimi}");
            //    Console.WriteLine(a+ " " +b);

            //}

            //    static string ToProper(string sona)
            //    {

            //    //if (sona == "") return "";

            //    return
            //            sona == "" ? "" :
            //            sona.Substring(0, 1).ToUpper() + sona.Substring(1).ToLower();

            //    }

            //static int Liida(int x, int y)
            //{
            //    return x + y; 

            #endregion

            int[] arvud = new int[51];
            Shuffle(arvud);

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i + 1;
            }

            foreach (var x in arvud)
            {
                if (x % 13 == 12)
                {
                    Console.Write($"\n{arvud[x]}");
                }
                else
                {
                    Console.Write($"\t{arvud[x]}");
                }
            }

        }
        public static void Shuffle(int[] arr)
        {
            // создаем экземпляр класса Random для генерирования случайных чисел
            Random rand = new Random();

            for (int i = arr.Length - 1; i >= 1; i--)
            {
                int j = rand.Next(i + 1);

                int tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }

        //public static void shuffleArray(int[] a)

        //{
        //    int n = a.Length;
        //    Random r = new Random();

        //    for (int i = 0; i < n; i++)
        //    {
        //        swap(a, i, i + r.Next(n - i));
        //    }
        //}

        //public static void swap(int[] arvud, int a, int b)
        //{
        //    int temp = arvud[a];
        //    arvud[a] = arvud[b];
        //    arvud[b] = temp;
        //}
    }
}


