﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace third_day
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Theory Aleks

            //string name = "Aleksandr";
            //string surename = "Sagalajev";

            //string jutt = "Ta utles,et \"tule siia\""; // \ - annab teada et jarmised \ ei oma tahtsust

            //Console.WriteLine(jutt);

            //string failinimi = "c:\\henn\\miski.txt";

            //Console.WriteLine(failinimi);

            //  \n - new line
            //  \t - tabulator
            //  \r - carige return -  naidis - "Aleks see my name\rMina" - tulemus - Mina see my name

            //long l = 17;
            //int i = (int)l; // casting operation - muutub int longiks kui paneme sulgudesse tuup 
            //long l2 = i; // imumatta
            //Console.WriteLine(i);

            //string arv = "123";
            //int arv = int.Parse(arv); // kasutame Parse selleks et tegutseda sellega

            //arv = Convert.ToInt32(arv); // konverdime kasutades Convert.ToInt

            //Console.WriteLine(arv);

            //(Console.ReadLine(), out int palk) // saab muutuja oomistada sellele mida kasutaja on sisendatud

            //writeline = viskab jargmisele reale
            //write = EncodingInfo viska 
            //readline() = votab mis on sisestatud stringina , .ToLower() - teeb vaikseteks tahtedeks enne vordsustamist, .ToUpper() - tekitab suured enne vordsustamist.
            //readkey = ootab kuni mingi klahv on vajutatud // (true) - ei naita vajutatud klahvi

            //const int SORMED = 5; // SUURED TAHEN KONSTANTIDEL
            //Console.WriteLine(SORMED);

            #endregion

            #region Lord identification system

            //Console.Write("Anna keisri nimi : ");
            //string name = Console.ReadLine();
            //if (name == "Napoleon")
            //{
            //    Console.WriteLine("Ta on Rooma keiser");
            //}
            //else if (name == "Cesar")
            //{
            //    Console.WriteLine("Ta on Rooma keiser");
            //}
            //else if (name == "Karl Suur")
            //{
            //    Console.WriteLine("Ta on Rooma keiser");
            //}
            //else if (name == "Aleksandr")
            //{
            //    Console.WriteLine("Ta on Rooma keiser");
            //}
            //else if (name == "Henn")
            //{
            //    Console.WriteLine("Noh peaaegu keiser :D");
            //}
            //else
            //{
            //    Console.WriteLine("Pole tegemist keisriga");
            //}

            #endregion


            #region Swich app for weekdays

            //Console.WriteLine("What day is it today ? : ");
            //string day = Console.ReadLine();
            //switch (day)
            //{
            //    case " sunday":
            //        Console.WriteLine("1");
            //        Console.WriteLine("1");
            //        break;

            //    case "sekond":
            //    case "monday":
            //        Console.WriteLine("2");
            //        Console.WriteLine("2");
            //        break;

            //    default:
            //        Console.WriteLine("Jookseme");
            //        break;
            //}
            #endregion


            #region SelfLerning ( traffic_light )

            Console.Write("Millist varvi valgusfoor ? : ");
            string color = Console.ReadLine().ToUpper(); // Kui kasutad SUURED TAHED vordluses siis pead kasutama .ToUpper()!!!
                                                         //SWITCH solution

            //switch (roadblinker)
            //{
            //    case "GREEN":
            //        Console.WriteLine(" DRIVE PLEASE ! ");
            //        break;

            //    case "YELLOW":
            //        Console.WriteLine(" PREPARE TO STOP ! ");
            //        break;

            //    case "RED":
            //        Console.WriteLine(" NO ENTRY !!! ");
            //        break;

            //    default:

            //        Console.WriteLine(" YOU JUST ON THE ROAD ");

            //        break;
            //}

            // IF solution

            //if (color == "GREEN"||color == "ROHELINE")
            //{
            //    Console.WriteLine("START");
            //}
            //else if (color == "YELLOW" || color == "KOLLANE")
            //{
            //    Console.WriteLine("PREPARE");
            //}
            //else if (color == "RED" || color == "PUNANE")
            //{
            //    Console.WriteLine("STOP !!!");
            //}
            //else
            //{
            //    Console.WriteLine("ON THE ROAD");
            //} 

            // GOTO CASE solution 

            // Console.Write("Millised tood teeme ? : ");
            // string work = Console.ReadLine().ToLower();
            // switch (work)
            //{
            //    case "welding":
            //        Console.WriteLine("Mast be prepared workpeace");
            //        break;
            //    case "CNC":
            //        Console.WriteLine("Must be roughted premilled");
            //        break;
            //    case "milling":
            //        Console.WriteLine("Must be d profile");
            //        goto case "CNC";
            //    case "turning":
            //        Console.WriteLine("Must be o profil");
            //        goto case "CNC";

            //    default:
            //        Console.WriteLine("Must be identified work");
            //        break;

            //}

            #endregion

            //Massiivid 

            //int[] - uherealine massiv

            //int[][] - kaherealine massiiv
                

            //   int [] arvud = { 1, 2, 33};

            //   int [] arvud2 = new int[3];

            //Console.WriteLine(arvud[3]);
                



        }
    }
}
